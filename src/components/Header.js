import React from 'react'
import '../components/Header.css';


function Header() {
    
    return (
        <div>
            <div className="header">
                <div className="madeBy">Made by: Milos</div>
                <div className="headerTitle"><span role="image" aria-label="joker card">🃏</span>21 card trick<span role="image" aria-label="joker card">🃏</span></div>
                <div className="headerSource">Source at: github</div>
            </div>
                <div className="mainTitle">Memorize a card... any card
            </div>
        </div>
    )
}

export default Header
