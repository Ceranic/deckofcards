import React from 'react'
import './Pile.css'

function Pile(props) {

    const selectPile = (x) => {
        
        var piles = [props.allCards.slice(0,7),
            props.allCards.slice(7,14),
            props.allCards.slice(14,21)];

        var centerPile = piles[x];
        var newPile = [];
        piles.splice(x,1);
        newPile.push(...piles[0],...centerPile,...piles[1]);
        var chosenPile = [...(newPile.filter((x,y) => y%3===0)),...(newPile.filter((x,y) => y%3===1)),...(newPile.filter((x,y) => y%3===2))];
   
        props.setAllCards(chosenPile);
        props.setPileCount(props.pileCount+1);

    }    
    
    return (
        <div className="pile-col" onClick={() => selectPile(props.id)}>
            {props.cards.map((x,y,z) => <img key={y} src={x.src} alt={z.alt}></img>)}
        </div>
    )
}

export default Pile
