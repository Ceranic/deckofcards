import React,{useState, useEffect} from 'react'
import Pile from './Pile.js'
import '../components/Cards.css'

function Cards(props) {
    var cardSymbol = ['H','C','S','D'];
    var cardsNumbers = ['A','2','3','4','5','6','7','8','9','0','J','Q','K'];

    const [cards,setCards] = useState([]);
    const [isChosen, setIsChosen] = useState(false);
    const [pileCount,setPileCount] = useState(0);

    const generateCards = () =>{
        var genArray = [];
        for(let i = 0;i<cardSymbol.length;i++){
            for(let j = 0;j<cardsNumbers.length;j++){
                if(cardSymbol[i] === 'D' && cardsNumbers[j] === 'A')continue;
                genArray.push({
                    symbol: cardSymbol[i],
                    number: cardsNumbers[j],
                    src: `https://deckofcardsapi.com/static/img/${cardsNumbers[j] + cardSymbol[i]}.png`
                })
            }
        }

        //Gen 21 card
        var newGenArray = [];
        for(let i = 0; i < 21; i++){
            var el = genArray.splice(Math.floor(Math.random() * genArray.length),1);
            newGenArray.push(...el);
        }
        setCards(newGenArray);
    }
    
    useEffect(() => {
        generateCards();
    },[]);
    return (
        <div className="generatedDiv">
            {isChosen === true ? '' : <button className="generatedButton" onClick={() => setIsChosen(true)}>Start</button>}
            <br></br>
            {pileCount === 3 ? (<div className="choosenCard">
                <img src={cards[10].src}></img>
            </div>) : (isChosen ? (<div className="pile-1">
                <Pile pileCount={pileCount} setPileCount={setPileCount} id={0} allCards={cards} setAllCards={setCards} cards={cards.filter((x,y) => y >= 0 && y < 7)}/>
                <Pile pileCount={pileCount} setPileCount={setPileCount} id={1} allCards={cards} setAllCards={setCards} cards={cards.filter((x,y) => y >= 7 && y < 14)}/>
                <Pile pileCount={pileCount} setPileCount={setPileCount} id={2} allCards={cards} setAllCards={setCards} cards={cards.filter((x,y) => y >= 14 && y < 21)}/>
            </div>) : <div className="generatedCards">{cards.map((x,y) => <img key={y} src={x.src}></img>)}</div>)}
            
            
        </div>
    )
}

export default Cards
